//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path          = require('path');
let movementsJSON = require('./movementsv2.json');
let bodyparser    = require('body-parser');

app.use(bodyparser.json());
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//base URL, function
app.get('/', function (req, res){

  res.sendFile(path.join(__dirname,'index.html'));

});

app.post('/', function (req, res){

  res.send('We have received your POST request changed');

});

app.put('/', function (req, res){

  res.send('We have received your PUT request');

});

app.delete('/', function (req, res){

  res.send('We have received your DELETE request');

});

app.get('/Customers/:idcustomer', function (req, res){

  res.send('Here is the customer number ' + req.params.idcustomer);

});

//sendfile: JSON
app.get('/Movements', function (req, res){

  res.sendfile('movementsv1.json');

});

//sendfile: JSON
app.get('/v2/Movements', function (req, res){

  res.json(movementsJSON);

});

//sendfile: JSON
app.get('/v2/Movements/:id', function (req, res){

  console.log(req.params.id);
  res.send(movementsJSON[req.params.id - 1]);

});

//query params
app.get('/v2/Movementsq', function (req, res){

  console.log(req.query);
  res.send('Received query ' + req.query);

});

//POST
app.post('/v2/Movements', function (req, res){

  let newmovement = req.body;
  newmovement.id = movementsJSON.length + 1;

  movementsJSON.push(newmovement);
  res.send('Movement success');

});
